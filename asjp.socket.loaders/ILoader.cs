﻿using System.Collections.Generic;
using asjp.socket.plugin;

namespace asjp.socket.loaders
{
    public interface ILoader
    {
        IEnumerable<T> Load<T>(string pathOrName) where T : IPlugin;
        IEnumerable<T> Load<T>(string pathOrName, Dictionary<string, string> environment) where T : IPlugin;
    }
}
