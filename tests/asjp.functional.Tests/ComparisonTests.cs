using asjp.hStdLib;
using NUnit.Framework;

namespace asjp.containers.Tests
{
    public class ComparisonTests{
        [Test]
        public void Equals_CreatesDelegateThatPassesWhenArgumentEquals(){
            var target = 123.EqualsTo();

            Assert.IsTrue(target(123));
            Assert.IsFalse(target(124));
        }
        [Test]
        public void LessOrEqual_CreatesDelegateThatPassesWhenArgumentNotGreater(){
            var target = 123.LessOrEqualTo();

            Assert.AreEqual(true, target(122));
            Assert.AreEqual(true, target(123));
            Assert.AreEqual(false, target(124));
        }
        [Test]
        public void Greater_CreatesDelegateThatPassesWhenArgumentIsGreater(){
            var target = 123.GreaterThan();

            Assert.AreEqual(false, target(122));
            Assert.AreEqual(false, target(123));
            Assert.AreEqual(true, target(124));
        }
        [Test]
        public void GreaterOrEqual_CreatesDelegateThatPassesWhenArgumentIsNotLess(){
            var target = 123.GreaterOrEqualTo();

            Assert.AreEqual(false, target(122));
            Assert.AreEqual(true, target(123));
            Assert.AreEqual(true, target(124));
        }
        [Test]
        public void Less_CreatesDelegateThatPassesWhenArgumentIsLess(){
            var target = 123.LessThan();

            Assert.AreEqual(true, target(122));
            Assert.AreEqual(false, target(123));
            Assert.AreEqual(false, target(124));
        }
    }
}