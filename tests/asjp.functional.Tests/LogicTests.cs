using System;
using static asjp.hStdLib.FunctionalLogic;
using NUnit.Framework;

namespace asjp.containers.Tests
{
    public class LogicTests{
        [Test]
        public void Not_CreatesInversedFunction(){
            var target = new Func<int, bool>(x => x == 123);

            var inverse = target.Not();

            Assert.IsTrue(target(123));
            Assert.IsFalse(inverse(123));
            Assert.IsTrue(inverse(125));
        }

        [Test]
        public void Or_CreatesFuncPassingOnAnyOfParameters(){
            var is123 = new Func<int, bool>(x => x == 123);
            var lessThan250 = new Func<int, bool>(x => x < 250);

            var result = is123.Or(lessThan250);

            Assert.IsTrue(is123(123));
            Assert.IsTrue(lessThan250(124));

            Assert.IsTrue(result(123));
            Assert.IsTrue(result(124));
            Assert.IsFalse(result(500));
        }

        [Test]
        public void And_CreatesFuncPassingOnBothParameters(){
            var graterThan123 = new Func<int, bool>(x => x > 123);
            var lessThan250 = new Func<int, bool>(x => x < 250);

            var result = graterThan123.And(lessThan250);

            Assert.IsTrue(graterThan123(124));
            Assert.IsTrue(lessThan250(124));

            Assert.IsTrue(result(125));
            Assert.IsFalse(result(120));
            Assert.IsFalse(result(500));
        }
    }
}