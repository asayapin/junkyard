﻿using NUnit.Framework;
using System.Linq;
using System;
using asjp.hStdLib;

namespace asjp.containers.Tests
{
    public class MaybeTests
    {
        [Test]
        public void Const_OnAnyRightValue_ReturnsLeftValue(){
            Assert.IsTrue(new object[]{1, "", ' ', false}.Select(123.Const<int, object>()).All(x => x == 123));
        }
        [Test]
        public void Id_ReturnsParameter(){
            Assert.AreEqual(123, 123.Id()());
        }
        [Test]
        public void Flip_CreatesFunctionWithFlippedParameters(){
            var target = new Func<int, string, bool>((x, s) => int.TryParse(s, out var t) ? t == x : false);

            var flipped = target.Flip();

            Assert.AreEqual(target(123, "123"), flipped("123", 123));
        }
    }
}