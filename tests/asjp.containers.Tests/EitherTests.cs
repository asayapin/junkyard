using NUnit.Framework;
using asjp.containers;
using System;

namespace asjp.containers.Tests
{
    public class EitherTests{
        [Test]
        public void Lift_LiftTheRightSide(){
            Either<int, string> 
                a = 123,
                b = Either<int, string>.LiftLeft(234),
                c = Either<int, string>.LiftRight("123");

                Assert.IsTrue(a.IsLeft);
                Assert.IsTrue(b.IsLeft);
                Assert.IsFalse(c.IsLeft);

                Assert.AreEqual(123, a.Left);
                Assert.AreEqual(null, a.Right);
                Assert.AreEqual(234, b.Left);
                Assert.AreEqual(null, b.Right);
                Assert.AreEqual(0, c.Left);
                Assert.AreEqual("123", c.Right);
        }
    }
}