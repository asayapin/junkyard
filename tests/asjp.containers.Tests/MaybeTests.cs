﻿using NUnit.Framework;
using asjp.containers;
using System;

namespace asjp.containers.Tests
{
    public class MaybeTests
    {
        [Test]
        public void Lift_CreatesFilledMaybe(){
            Maybe<int> a = 123,
                    b = Maybe<int>.Lift(123);

            Assert.IsTrue(a.HasValue);
            Assert.IsTrue(b.HasValue);
        }

        [Test]
        public void None_ProvidesEmptyMaybe(){
            Assert.IsFalse(Maybe<int>.None.HasValue);
        }

        [Test]
        public void Bind_OnFilledMaybe_ProcessesValue(){
            Assert.AreEqual(200, Maybe<int>.Lift(100).Bind<int>(x => x * 2).Value);
        }

        [Test]
        public void Bind_OnEmptyMaybe_SkipsAction(){
            Assert.IsFalse(Maybe<int>.None.Bind<bool>(x => throw new Exception()).HasValue);
        }
    }
}