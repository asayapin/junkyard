using System;

namespace asjp.hStdLib
{
    public static class FunctionalComparison{
        public static Func<T, bool> EqualsTo<T>(this T value) => x => object.Equals(x, value);
        public static Func<T, bool> GreaterThan<T>(this T value) where T : IComparable<T> => x => x.CompareTo(value) > 0;
        public static Func<T, bool> GreaterOrEqualTo<T>(this T value) where T : IComparable<T> => x => x.CompareTo(value) >= 0;
        public static Func<T, bool> LessThan<T>(this T value) where T : IComparable<T> => x => x.CompareTo(value) < 0;
        public static Func<T, bool> LessOrEqualTo<T>(this T value) where T : IComparable<T> => x => x.CompareTo(value) <= 0;
    }

    public static class FunctionalOps{
        public static R Then<T, R>(this T value, Func<T, R> map) => map(value);
    }
}
