﻿using System;

namespace asjp.hStdLib
{
    public static class FunctionalStdLib
    {
        public static Func<T> Id<T>(this T value) => () => value;
        public static Func<U, T> Const<T, U>(this T value) => _ => value;
        public static Func<T2, T1, R> Flip<T1, T2, R>(this Func<T1, T2, R> f) => (b, a) => f(a, b);
    }
}
