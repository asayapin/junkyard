using System;

namespace asjp.hStdLib
{
    public static class FunctionalLogic{
        public static Func<T, bool> Not<T>(this Func<T, bool> f) => x => !f(x);
        public static Func<T, bool> And<T>(this Func<T, bool> left, Func<T, bool> right) => x => left(x) && right(x);
        public static Func<T, bool> Or<T>(this Func<T, bool> left, Func<T, bool> right) => x => left(x) || right(x);
    }
}
