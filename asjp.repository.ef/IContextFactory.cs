﻿using Microsoft.EntityFrameworkCore;

namespace asjp.repository.ef
{
    public interface IContextFactory<T> where T : DbContext
    {
        T GetContext();
    }
}
