﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using asjp.containers;
using Microsoft.EntityFrameworkCore;

namespace asjp.repository.ef
{
    public abstract class EfRepository<T, TKey, TContext> 
        : IRepository<T, TKey> 
        where T : class, IEntity<TKey>
        where TKey : IEquatable<TKey> 
        where TContext : DbContext
    {
        private readonly IContextFactory<TContext> _contextFactory;
        private readonly Func<TContext, DbSet<T>> _setGetter;

        protected EfRepository(IContextFactory<TContext> contextFactory, Func<TContext, DbSet<T>> setGetter)
        {
            _contextFactory = contextFactory;
            _setGetter = setGetter;
        }

        public virtual IQueryable<T> LoadDataOnRead(DbSet<T> set) => set;
        public virtual IQueryable<T> LoadDataOnGet(DbSet<T> set) => set;

        public virtual async Task<Maybe<T>> GetAsync(TKey key)
        {
            using var context = _contextFactory.GetContext();
            var set = _setGetter(context);
            var result = await LoadDataOnGet(set).FirstOrDefaultAsync(x => x.Key.Equals(key));
            return Equals(default(T), result) ? Maybe<T>.None : Maybe<T>.Lift(result);
        }

        public virtual async Task<IEnumerable<T>> ReadAsync()
        {
            using var context = _contextFactory.GetContext();
            var set = _setGetter(context);
            return await LoadDataOnRead(set).ToListAsync();
        }

        public virtual async Task<Either<T, Exception>> AddAsync(T item)
        {
            try
            {
                item.Key = default;
                using var context = _contextFactory.GetContext();
                var set = _setGetter(context);
                await set.AddAsync(item);
                await context.SaveChangesAsync();
                return Either<T, Exception>.LiftLeft(item);
            }
            catch (Exception e)
            {
                return Either<T, Exception>.LiftRight(e);
            }
        }

        public virtual async Task<Either<T, Exception>> UpdateAsync(T item)
        {
            try
            {
                using var context = _contextFactory.GetContext();
                var set = _setGetter(context);
                var entry = set.Update(item);
                if (entry.State == EntityState.Added)
                    return Either<T, Exception>.LiftRight(
                        new KeyNotFoundException("Entity with supplied key is not found"));
                await context.SaveChangesAsync();
                return Either<T, Exception>.LiftLeft(entry.Entity);
            }
            catch (Exception e)
            {
                return Either<T, Exception>.LiftRight(e);
            }
        }

        public virtual async Task<Either<T, Exception>> UpsertAsync(T item)
        {
            try
            {
                using var context = _contextFactory.GetContext();
                var set = _setGetter(context);
                var entry = set.Update(item);
                await context.SaveChangesAsync();
                return Either<T, Exception>.LiftLeft(entry.Entity);
            }
            catch (Exception e)
            {
                return Either<T, Exception>.LiftRight(e);
            }
        }

        public virtual async Task<Either<bool, Exception>> DeleteAsync(TKey key)
        {
            try
            {
                using var context = _contextFactory.GetContext();
                var set = _setGetter(context);
                set.Remove(await set.FindAsync(key));
                await context.SaveChangesAsync();
                return Either<bool, Exception>.LiftLeft(true);
            }
            catch (Exception e)
            {
                return Either<bool, Exception>.LiftRight(e);
            }
        }
    }
}