﻿namespace asjp.socket.plugin
{
    public interface IPluginInfo
    {
        string Name { get; }
        string Description { get; }
        string Version { get; }
    }
}