﻿using System.Collections.Generic;

namespace asjp.socket.plugin
{
    public interface IPlugin : IPluginInfo
    {
        void Init(Dictionary<string, string> environment);  // TODO : env?
        bool CanLoad(Dictionary<string, string> environment);
    }
}
