﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using asjp.containers;
using Microsoft.Azure.Cosmos;

namespace asjp.repository.cosmosDb
{
    public abstract class CosmosDbRepository<T, TKey>
        : IRepository<T, TKey>
        where T : class, IEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        private readonly Container _cosmosContainer;
        private readonly PartitionKey _partitionKey;

        public CosmosDbRepository(Container cosmosContainer, string partitionKeyPath)
        {
            _cosmosContainer = cosmosContainer;
            _partitionKey = new PartitionKey(partitionKeyPath);
        }

        public static async Task<Container> GetContainer(string uri, string key, string dbName,
            string containerName, string partitionKeyPath = "/id")
        {
            var client = new CosmosClient(uri, key);
            var database = await client.CreateDatabaseIfNotExistsAsync(dbName);
            var containerProps = new ContainerProperties(containerName, partitionKeyPath);
            var container = await database.Database.CreateContainerIfNotExistsAsync(containerProps);
            return container.Container;
        }

        public virtual async Task<Maybe<T>> GetAsync(TKey key)
        {
            try
            {
                var resource = (await _cosmosContainer.ReadItemAsync<T>(key.ToString(), _partitionKey)).Resource;
                return Maybe<T>.Lift(resource);
            }
            catch
            {
                return Maybe<T>.None;
            }
        }

        public virtual Task<IEnumerable<T>> ReadAsync()
        {
            return Task.FromResult(_cosmosContainer.GetItemLinqQueryable<T>().AsEnumerable());
        }

        public virtual async Task<Either<T, Exception>> AddAsync(T item)
        {
            try
            {
                return Either<T, Exception>.LiftLeft((await _cosmosContainer.CreateItemAsync(item)).Resource);
            }
            catch (Exception e)
            {
                return Either<T, Exception>.LiftRight(e);
            }
        }

        public virtual async Task<Either<T, Exception>> UpdateAsync(T item)
        {
            try
            {
                var response = await _cosmosContainer.ReadItemAsync<T>(item.Key.ToString(), _partitionKey);
                if (response.StatusCode == HttpStatusCode.OK)
                    return await UpsertAsync(item);
                return Either<T, Exception>.LiftRight(new HttpRequestException(response.StatusCode.ToString()));
            }
            catch (Exception e)
            {
                return Either<T, Exception>.LiftRight(e);
            }
        }

        public virtual async Task<Either<T, Exception>> UpsertAsync(T item)
        {
            var response = (await _cosmosContainer.UpsertItemAsync(item));
            return response.StatusCode == System.Net.HttpStatusCode.OK 
                ? Either<T, Exception>.LiftLeft(response.Resource) 
                : Either<T, Exception>.LiftRight(new HttpRequestException(response.StatusCode.ToString()));
        }

        public virtual async Task<Either<bool, Exception>> DeleteAsync(TKey key)
        {
            try
            {
                await _cosmosContainer.DeleteItemAsync<T>(key.ToString(), _partitionKey);
                return Either<bool, Exception>.LiftLeft(true);
            }
            catch (Exception e)
            {
                return Either<bool, Exception>.LiftRight(e);
            }
        }
    }
}
