using System;
using System.Threading.Tasks;

namespace asjp.containers.monadicExtensions
{
    public static class EitherExtensions{
        public static R Check<TL, TR, R>(this Either<TL, TR> e, Func<TL, R> ifLeft, Func<TR, R> ifRight){
            return e.IsLeft ? ifLeft(e.Left) : ifRight(e.Right);
        }

        public static Func<Either<T, Exception>> WrapToEither<T>(Func<T> f){
            return () => {
                try
                {
                    return f();
                }
                catch (Exception e)
                {
                    return Either<T, Exception>.LiftRight(e);
                }
            };
        }

        public static Func<Task<Either<T, Exception>>> WrapTaskToEither<T>(Func<Task<T>> f){
            return async () => {
                try
                {
                    return await f();
                }
                catch (Exception e)
                {
                    return Either<T, Exception>.LiftRight(e);
                }
            };
        }

        public static EitherFixed<T> Fix<T>() => new EitherFixed<T>();
    }

    public class EitherFixed<T>{
        public Either<TL, T> LiftLeft<TL>(TL value) => Either<TL, T>.LiftLeft(value);
        public Either<T, TR> LiftRight<TR>(TR value) => Either<T, TR>.LiftRight(value);
    }
}
