using System;

namespace asjp.containers.monadicExtensions
{
    public static class UnitExtensions {
        public static Func<T, Unit> Wrap<T>(Action<T> action)
        {
            return arg =>
            {
                action(arg);
                return Unit.Instance;
            };
        }

        public static Func<Unit> Wrap(Action action)
        {
            return delegate
            {
                action();
                return Unit.Instance;
            };
        }

        public static Func<Unit> Noop { get; } = () => Unit.Instance;
    }
}
