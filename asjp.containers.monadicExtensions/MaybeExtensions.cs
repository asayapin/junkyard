﻿using System;
using System.Threading.Tasks;

namespace asjp.containers.monadicExtensions
{
    public static class MaybeExtensions
    {
        public static R Check<T, R>(this Maybe<T> m, Func<T, R> ifPresent, Func<R> ifAbsent)
        {
            return m.HasValue
                ? ifPresent(m.Value)
                : (ifAbsent ?? Nop<R>())();
        }

        public static Maybe<R> Bind<T, R>(this Maybe<T> m, Func<T, R> whenPresent)
        {
            return m.HasValue 
                ? whenPresent(m.Value)
                : Maybe<R>.None;
        }

        private static Func<R> Nop<R>() => () => default;

        public static Func<Maybe<T>> WrapToMaybe<T>(Func<T> f, bool treatDefaultAsNone) {
            return () => {
                try{
                    var ret = f();
                    if (object.Equals(default(T), ret) && treatDefaultAsNone) return Maybe<T>.None;
                    return ret;
                }
                catch {
                    return Maybe<T>.None;
                }
            };
        }

        public static Func<Task<Maybe<T>>> WrapTaskToMaybe<T>(Func<Task<T>> f, bool treatDefaultAsNone) {
            return async () => {
                try{
                    var ret = await f();
                    if (object.Equals(default(T), ret) && treatDefaultAsNone) return Maybe<T>.None;
                    return ret;
                }
                catch {
                    return Maybe<T>.None;
                }
            };
        }
    }
}
