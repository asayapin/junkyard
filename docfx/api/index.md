# Pet projects

Currently added:

- asjp.repository - repository pattern implementation with EF Core & Azure Cosmos Db providers
- asjp.containers - monadic containers (**Maybe** & **Either**)
- asjp.socket - simple plugin system for .Net Core