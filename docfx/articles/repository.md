# asjp.repository

Repository boilerplate

**Purpose**: simplify work with different data sources, encapsulate EF DataContext's so that business logic is not cluttered with storage implementation details, provide simple async api

## Usage

Any usage requires you to implement `IEntity` interface with single `Key` property. Property should be decorated with `[NotMapped]` to skip it on storage schema generation.

Raw repository usage requires implementation of `IRepository` and its' methods (which is fairly simple)

Also you can use `EfRepository` as a base class for your concrete repository, which requires you to provide:
- context factory, 
- DbSet getter delegate

Also you can override virtual loading methods to include navigation properties.

CosmosDb repository provides method `Init`, that can be used to obtain cosmos container instance needed to initialize repository. 
Init method requires CosmosDb instance uri & key, db name and container name, and container partition key