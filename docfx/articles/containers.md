# asjp.containers project

Content:
- **Maybe** monad implementation
- **Either** monad implementation
- Extensions to work with task

**Rationale**: null-reference exceptions, `if`s, `try`-`catch`s... All that is surely an additional headache when designing a complex system,
as well as additional noise for code perception. What if I want to create API that is *obviously* producing an exception, without using xml-doc?

Another case - many repository implementations just return null in case object not found. LINQ return null if object not found (if you explicitly allow such a behavior - otherwise, catch an exception). Why should I always make additional efforts for remembering that references may be null?

**Why not C# 8**: because BCL is not covered with `[NotNull]` annotations yet; because you can override check with `!`; because you can break things on other levels. 

## Usage

Is fairly simple:

**Maybe**:

```c#

var emptyMaybe = Maybe<MyType>.Empty;
var maybeMyType = Maybe<MyType>.FromValue(new MyType());

emptyMaybe.Bind(
    Unit.Wrap(Console.WriteLine),
    Unit.Noop
);

string x = maybeMyType.Bind(value => value.ToString());

```

**Either**:

```c#
var either1 = Either<int, string>.FromFirstInstance(42);
var either2 = Either<int, string>.FromSecondInstance("43");

int res = either1.Bind(
    intVal => intVal++,
    int.Parse
);
```

### Task extensions

There are simple extension methods for following purposes:
- wrap container in task - almost no overhead, as wrapping happens with `Task.FromResult`
- bind to container wrapped in task - allows following code:

```c#
Task<Either<string, Exception>> SafeWebRequest(string uri){
    try {
        string result = // fetch resource
        return Either<string, Exception>.FromFirstInstance(result);
    }
    catch (Exception e) {
        return Either<string, Exception>.FromSecondInstance(e);
    }
}

// consuming function above

var fetchAndProcessTask = SafeWebRequest("http://example.com")
    .Then(
        result => result.Length,
        exception => 0
    );
var resultLength = await fetchAndProcessTask;
```