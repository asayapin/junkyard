﻿using System;
using System.Threading.Tasks;
using asjp.containers.monadicExtensions;

namespace asjp.containers.taskExtensions
{
    public static class Extensions
    {
        public static Task<R> Then<T, R>(this Task<Maybe<T>> task, Func<T, R> whenPresent, Func<R> whenAbsent)
        {
            return task.ContinueWith<R>(x => x.Result.Check(whenPresent, whenAbsent));
        }

        public static Task<R> Then<T1, T2, R>(this Task<Either<T1, T2>> task, Func<T1, R> whenFirst,
            Func<T2, R> whenSecond)
        {
            return task.ContinueWith(x => x.Result.Check(whenFirst, whenSecond));
        }

        public static Task<Either<T1, T2>> AsTask<T1, T2>(this Either<T1, T2> either) => Task.FromResult(either);
        public static Task<Maybe<T>> AsTask<T>(this Maybe<T> maybe) => Task.FromResult(maybe);
    }
}
