﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using asjp.containers;

namespace asjp.repository
{
    public interface IRepository<T, in TKey> where T : IEntity<TKey> where TKey : IEquatable<TKey>
    {
        Task<Maybe<T>> GetAsync(TKey key);
        Task<IEnumerable<T>> ReadAsync();
        Task<Either<T, Exception>> AddAsync(T item);
        Task<Either<T, Exception>> UpdateAsync(T item);
        Task<Either<T, Exception>> UpsertAsync(T item);
        Task<Either<bool, Exception>> DeleteAsync(TKey key);
    }
}
