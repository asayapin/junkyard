﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace asjp.repository
{
    public interface IEntity<TKey> where TKey : IEquatable<TKey>
    {
        [NotMapped]
        TKey Key { get; set; }
    }
}