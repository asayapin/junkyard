﻿namespace asjp.containers
{
    public class Either<TL, TR>
    {
        private readonly object _value;
        private Either(object value, bool isFirst)
        {
            _value = value;
            IsLeft = isFirst;
        }
        public bool IsLeft { get; }
        public TL Left => IsLeft ? (TL)_value : default;
        public TR Right => IsLeft ? default : (TR)_value;

        public static Either<TL, TR> LiftLeft(TL instance) => new Either<TL, TR>(instance, true);
        public static Either<TL, TR> LiftRight(TR instance) => new Either<TL, TR>(instance, false);

        public static implicit operator Either<TL, TR> (TL value) => LiftLeft(value);
    }
}