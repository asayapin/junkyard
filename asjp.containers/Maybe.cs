﻿using System;

namespace asjp.containers
{
    public class Maybe<T>
    {
        public bool HasValue { get; }
        public T Value { get; }
        private Maybe(T value)
        {
            Value = value;
            HasValue = true;
        }
        private Maybe()
        {
            HasValue = false;
            Value = default;
        }

        public static Maybe<T> None { get; } = new Maybe<T>();
        public static Maybe<T> Lift(T value) => new Maybe<T>(value);
        public static implicit operator Maybe<T>(T value) => new Maybe<T>(value);

        public Maybe<R> Bind<R>(Func<T, Maybe<R>> map) => HasValue ? map(Value) : Maybe<R>.None;
    }
}
