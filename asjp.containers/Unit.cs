﻿using System;

namespace asjp.containers
{
    public sealed class Unit : IEquatable<Unit>
    {
        private Unit() { }
        public static Unit Instance { get; } = new Unit();
        public bool Equals(Unit other) => !(other is null);

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Unit) obj);
        }

        public override int GetHashCode() => 0;
    }
}
