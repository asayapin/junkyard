FROM mono:latest
RUN apt update -y && apt upgrade -y
# git
RUN apt install git -y
# dotnet
RUN apt install apt-transport-https -y &&\
    curl https://packages.microsoft.com/config/ubuntu/19.04/packages-microsoft-prod.deb -o packages-microsoft-prod.deb && \
    dpkg -i packages-microsoft-prod.deb &&\
    apt update -y &&\
    apt install dotnet-sdk-3.1 -y
# docfx 
RUN cd /usr/local/bin &&\
    nuget install docfx.console -version 2.51.0 &&\
    mv docfx.console.2.51.0/ docfx/ &&\
    echo '#!/bin/bash' >> /usr/bin/docfx &&\
    echo 'mono /usr/local/bin/docfx/tools/docfx.exe "$@"' >> /usr/bin/docfx &&\
    chmod +x /usr/bin/docfx
# npm
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - &&\
    apt install nodejs -y
# ncftp
RUN apt install -y ncftp